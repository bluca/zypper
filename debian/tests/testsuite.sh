#!/bin/sh
set -uxe

TEMP_DIR="$(mktemp -d)"

cat <<EOF >"${TEMP_DIR}/tl.repo"
[tl]
name=tl
baseurl=https://download.opensuse.org/tumbleweed/repo/oss/
gpgcheck=1
enabled=1
autorefresh=0
keeppackages=1
EOF

zypper --non-interactive --root "${TEMP_DIR}/newroot" --gpg-auto-import-keys addrepo "${TEMP_DIR}/tl.repo"
zypper --non-interactive --root "${TEMP_DIR}/newroot" --gpg-auto-import-keys install filesystem

test -d "${TEMP_DIR}/newroot/usr/lib"
