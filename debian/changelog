zypper (1.14.87-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.87'
  * Bump build dependency on libzypp to 17.36.4

 -- Luca Boccassi <bluca@debian.org>  Sun, 02 Mar 2025 15:49:27 +0000

zypper (1.14.84-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.84'

 -- Luca Boccassi <bluca@debian.org>  Mon, 17 Feb 2025 11:09:19 +0000

zypper (1.14.83-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.83'
  * Bump dependency on libzypp-dev

 -- Luca Boccassi <bluca@debian.org>  Tue, 11 Feb 2025 12:03:21 +0000

zypper (1.14.81-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.81'

 -- Luca Boccassi <bluca@debian.org>  Mon, 27 Jan 2025 11:35:17 +0000

zypper (1.14.80-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.80'

 -- Luca Boccassi <bluca@debian.org>  Sun, 19 Jan 2025 12:05:57 +0000

zypper (1.14.79-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.79'

 -- Luca Boccassi <bluca@debian.org>  Fri, 10 Jan 2025 18:45:15 +0000

zypper (1.14.78-1) unstable; urgency=medium

  * autopkgtest: ensure it runs when rpm is updated
  * Update upstream source from tag 'upstream/1.14.78'
  * autopkgtest: add needs-root restriction

 -- Luca Boccassi <bluca@debian.org>  Fri, 15 Nov 2024 10:53:01 +0000

zypper (1.14.77-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.77'
  * Bump dependency on libzypp-dev to 17.35.10
  * Do not run tests in parallel

 -- Luca Boccassi <bluca@debian.org>  Tue, 10 Sep 2024 12:56:07 +0200

zypper (1.14.76-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.76'

 -- Luca Boccassi <bluca@debian.org>  Sun, 07 Jul 2024 23:41:45 +0100

zypper (1.14.74-1) unstable; urgency=medium

  * Mark autopkgtest as superficial
  * Update upstream source from tag 'upstream/1.14.74'

 -- Luca Boccassi <bluca@debian.org>  Mon, 01 Jul 2024 09:53:29 +0100

zypper (1.14.73-3) unstable; urgency=high

  * Restrict autopkgtest to amd64, repositories not available on other
    arches

 -- Luca Boccassi <bluca@debian.org>  Thu, 16 May 2024 10:09:46 +0100

zypper (1.14.73-2) unstable; urgency=medium

  * Add autopkgtest smoke test
  * Bump dependency on libzypp-dev to 17.34

 -- Luca Boccassi <bluca@debian.org>  Wed, 15 May 2024 22:08:06 +0100

zypper (1.14.73-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.73'
  * Bump libzypp-dev dependency to >= 17.33

 -- Luca Boccassi <bluca@debian.org>  Sun, 05 May 2024 20:21:12 +0100

zypper (1.14.71-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.71'
  * Bump Standards-Version to 4.7.0, no changes

 -- Luca Boccassi <bluca@debian.org>  Sat, 20 Apr 2024 20:20:26 +0100

zypper (1.14.70-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.70'

 -- Luca Boccassi <bluca@debian.org>  Wed, 03 Apr 2024 23:50:02 +0100

zypper (1.14.69-1) unstable; urgency=medium

  * Move package to pkg-rpm-team
  * Update upstream source from tag 'upstream/1.14.69'
  * Bump dependency on libzypp to 17.32

 -- Luca Boccassi <bluca@debian.org>  Sat, 23 Mar 2024 23:00:56 +0000

zypper (1.14.68-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.68'

 -- Luca Boccassi <bluca@debian.org>  Wed, 20 Dec 2023 14:43:28 +0100

zypper (1.14.66-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/1.14.66'
  * d/control: drop ancient version constraint on CMake
  * Drop 1001_make-ZyppCommon-cmake-module-includable.patch, merged
    upstream

 -- Luca Boccassi <bluca@debian.org>  Fri, 13 Oct 2023 11:20:25 +0100

zypper (1.14.65-1) unstable; urgency=medium

  * Switch to git-buildpackage workflow
  * Switch to debhelper
  * Update upstream source from tag 'upstream/1.14.65'
  * Drop 1002_some-spelling-fixes.patch, merged upstream
  * Drop manual setting of CMake prefix directory
  * Drop manual rename of bash completion script, fixed upstream

 -- Luca Boccassi <bluca@debian.org>  Sun, 01 Oct 2023 12:40:10 +0100

zypper (1.14.63-1) unstable; urgency=medium

  * New upstream release 1.14.63
  * Mark zypper-common and zypper-doc as MA: foreign
  * d/copyright: use wildcards to shorten and fix Lintian warnings
  * d/copyright: update upstream-contact to project mailing list
  * Bash completion is now installed to /usr/share by cmake, adjust
    packaging

 -- Luca Boccassi <bluca@debian.org>  Sun, 27 Aug 2023 12:12:21 +0100

zypper (1.14.62-1) unstable; urgency=medium

  * New upstream release 1.14.62

 -- Luca Boccassi <bluca@debian.org>  Sun, 30 Jul 2023 13:52:29 +0100

zypper (1.14.61-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove unnecessary get-orig-source-target.
  * Update standards version to 4.6.1, no changes needed.

  [ Luca Boccassi ]
  * New upstream release 1.14.61 (Closes: #1040437)
  * Bump Standards-Version to 4.6.2, no changes
  * Drop 2002_locale-workaround-in-unit-tests.patch, fixed upstream
  * Refresh 1002_some-spelling-fixes.patch for 1.14.61
  * Add myself as uploader
  * Bump dependency on libzypp to 17.31

 -- Luca Boccassi <bluca@debian.org>  Thu, 06 Jul 2023 11:09:43 +0100

zypper (1.14.42-2) unstable; urgency=medium

  * debian/watch:
    + Fix Github watch URL.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 29 Apr 2021 15:04:12 +0200

zypper (1.14.42-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Trivial patch rebase.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 04 Feb 2021 00:24:42 +0100

zypper (1.14.41-1) unstable; urgency=medium

  * New upstream release.
    - Fix FTBFS against libicu-dev >= 66. (Closes: #959756).
    - Fix FTBFS against boost 1.71 (and 1.74). (Closes: #960427).
  * debian/patches:
    + Drop 0001_bash-port-of-zypper-log.patch. Applied upstream.
    + Drop 2001_build-tests-with-all.patch. Can now be achieved with
      CMake build option.
    + Trivial rebase of 2002_locale-workaround-in-unit-tests.patch.
    + Update / improve 1002_some-spelling-fixes.patch.
  * debian/control:
    + Update versioned B-D on cmake (>= 3.1).
    + Bump Standards-Version: to 4.5.1. No changes needed.
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Update versioned B-D on libzypp-dev (>= 17.25).
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/patches:
  * debian/rules:
    + Set -DENABLE_BUILD_TESTS switch to 'ON'.
  * debian/watch:
    + Switch to format version 4.
  * lintian:
    + Fix renamed-tag issue.
    + Drop groff-message override.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 06 Jan 2021 23:53:33 +0100

zypper (1.14.11-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001_bash-port-of-zypper-log.patch. Drop requirement of Python in
      zypper.
  * debian/control:
    + Drop from D (zypper): python. (Closes: #945728).
    + Bump Standards-Version: to 4.4.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 09 Jan 2020 10:58:53 +0100

zypper (1.14.11-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase 1002_some-spelling-fixes.patch and
      2002_locale-workaround-in-unit-tests.patch.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 21 Sep 2018 13:59:04 +0200

zypper (1.14.9-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file. Omit .gz files at
      auto-generation.
  * debian/control:
    + Bump Standards-Version: to 4.2.1. No changes needed.
    + Bump versioned B-D on libzypp to ">= 17.6.3".

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 31 Aug 2018 10:08:59 +0200

zypper (1.14.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.2.0. No changes needed.
    + Enforce building against latest libzypp. Bump versioned B-D to >= 17.6.1.
  * debian/rules:
    + Stop repacking the orig tarball. No clue why we started this.
  * debian/patches:
    + Rebase 1002_some-spelling-fixes.patch.
    + Update 1002_some-spelling-fixes.patch. More spelling issues found and
      fixed.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Aug 2018 16:59:52 +0200

zypper (1.14.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + Update Vcs-*: fields. Packaging Git has been migrated to salsa.debian.org.
    + Add S (zypper): zypper-doc. (Closes: 849982).
    + Switch to versioned B-D on libzypp (>= 17.0.2). (Closes: #812243).
    + Switch from Priority: extra to optional.
    + Add B-D: asciidoctor.
  * debian/{control,compat}:
    + Bump to DH compat level 10.
  * debian/rules: Create orig tarball in ../ rather then $(CURDIR).
    + Enable unit testing at build time.
    + Avoid using dpkg-parsechangelog.
    + Enable all hardening flags.
    + Move bash completion files to /usr/share/bash-completion/completions/.
      Drop .sh file suffix.
  * debian/{rules,zypper.examples}:
    + Don't install zypper wrapper scripts to replace APT to /usr/bin, rather
      ship them as examples.
  * debian/copyright:
    + Use secure URI to obtain copyright references.
    + Update copyright attributions.
    + Regenerate copyright.in for this upstream release.
  * debian/patches:
    + Drop 1003_fix-spelling-errors.patch. Applied upstream.
    + Add 2001_build-tests-with-all.patch. Upstream's ctest build target does
      not trigger the test code build. Dirty work around is building test code
      with the all target.
    + Add 2002_locale-workaround-in-unit-tests.patch. Work around failing unit
      test (Test #3: text_test) due to non-configured locales in sbuild build
      environment).
  * debian/patches:
    + Add 1002_some-spelling-fixes.patch. Thanks lintian.
  * debian/zypper.lintian-overrides:
    + Slightly adapt override manpage-has-errors-from-man.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 10 Jun 2018 11:35:49 +0200

zypper (1.12.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 1002_fix-spelling-errors.patch. Applied upstream.
    + Add 1003_fix-spelling-errors.patch. Fix some new spelling errors that
      appeared since last upload in the upstream sources.
  * debian/copyright{,.in}:
    + Update for this upstream release.
  * debian/control:
    + Bump Standards: to 3.9.6. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 16 Jun 2015 10:32:28 +0200

zypper (1.11.13-1) unstable; urgency=medium

  * Initial release. (Closes: #761990).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 30 Sep 2014 14:22:36 +0200
